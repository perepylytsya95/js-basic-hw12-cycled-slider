const allImages = document.querySelectorAll('.image-to-show');
const stopBtn = document.createElement('button');
const continueBtn = document.createElement('button');
stopBtn.innerText = 'Прекратить';
stopBtn.style.cssText =`
        margin: 0 20px 10px 0;
        font-size: 20px;
        background: red;
        cursor: pointer;
        border: 5px double;
`;
continueBtn.innerText = 'Возобновить показ';
continueBtn.style.cssText = `
        font-size: 20px;
        background: green;
        cursor: pointer;
        border: 5px double;
`;
continueBtn.setAttribute('disabled', '');
document.body.prepend(stopBtn, continueBtn);

for (let image of allImages){
        image.style.display = 'none';
        allImages[0].style.display = 'block';
}

let intervalId = setInterval(show,3000);
let count = 1;
function show (){
        if (count < allImages.length){
                for (let image of allImages){
                        image.style.display = 'none';
                }
                allImages[count].style.display = 'block';
                count += 1;
        } else {
                for (let image of allImages){
                        image.style.display = 'none';
                        allImages[0].style.display = 'block';
                }
                count = 1;
         }
}

stopBtn.addEventListener('click', () =>{
        clearInterval(intervalId);
        stopBtn.setAttribute('disabled','');
        continueBtn.removeAttribute('disabled');
})

continueBtn.addEventListener("click", ()=>{
        intervalId = setInterval(show,3000);
        continueBtn.setAttribute('disabled', '');
        stopBtn.removeAttribute('disabled');
})






